import os
import pygame
from pygame.locals import *

from const import *

from pgu import engine
from pgu import timer
from pgu import high

class Game(engine.Game):
    def init(self):
        self.timer = timer.Timer(FPS)
        self.mode = 0
        self.screen = pygame.display.set_mode((SW,SH),self.mode)
        pygame.font.init()
        try:
            pygame.mixer.init()
            self.sound = True
        except:
            self.sound = False
        pygame.display.set_caption("watermelons")
        self.music = 1
        self.high = high.High("melons.hs",HIGHS)
        self.score = None
        if len(self.high) == 0:
            for name in ['pekuja','philhassey','treeform','watermelons','tramoplines','pyweek','pygame','pgu','cuzco','apples','grass','trees','stuff','things','blah']:
                self.high.submit(0,name)

    def tick(self):
        self.timer.tick()
        
    def event(self,e):
        if e.type is QUIT:
            self.state = engine.Quit(self)
            return 1
        
        if e.type is KEYDOWN and e.key == K_F10:
            tmp = pygame.Surface((SW,SH)).convert()
            tmp.blit(self.screen,(0,0))
            self.mode ^= FULLSCREEN
            self.screen = pygame.display.set_mode((SW,SH),self.mode)
            self.screen.blit(tmp,(0,0))
            pygame.display.flip()
            return 1
        
        if e.type is KEYDOWN and e.key == K_F2:
            if self.sound:
                self.music ^= 1
                pygame.mixer.music.set_volume(self.music)
            return 1
            

g = Game()
import menu
g.run(menu.Menu(g))
#g.run(menu.Highs(g))

pygame.quit()


