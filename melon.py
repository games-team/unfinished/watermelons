import os
import pygame
import random

class _Sound:
    def play(self):
        pass

def sound(fname):
    if pygame.mixer.get_init():
        return pygame.mixer.Sound(fname)
    return _Sound()

class Melon(pygame.sprite.Sprite):
    grow_animation = []
    bounce_animation = []
    image = None
    squish_image = None
    smash_animation = []
    bounce_sound = None
    smash_sound = None
    fall_sound = None
    grow_sound = None
    HANGING = 0
    JIGGLING = 1
    FALLING = 2
    BREAKING = 3
    BOUNCING = 4
    GROWING = 5
    GROWING2 = 6
    num_melons = 0

    def __init__(self,timer):
        pygame.sprite.Sprite.__init__(self)

        Melon.num_melons += 1
        
        self.timer = timer

        if not Melon.image:
            Melon.bounce_sound = sound(os.path.join("data","bounce.ogg"))
            Melon.smash_sound = sound(os.path.join("data","smash.ogg"))
            Melon.fall_sound = sound(os.path.join("data","fall.ogg"))
            Melon.grow_sound = sound(os.path.join("data","grow.ogg"))
            Melon.image = pygame.image.load(os.path.join("data","mellon0005.png"))
            Melon.squish_image = pygame.image.load(os.path.join("data","mellon0011.png"))
            Melon.smash_animation.append(pygame.image.load(os.path.join("data","mellon0013.png")))
            Melon.smash_animation.append(pygame.image.load(os.path.join("data","mellon0012.png")))

            for i in range(5):
                Melon.grow_animation.append(pygame.image.load(os.path.join("data","mellon000" + str(i + 1) + ".png")))
            for i in range(3):
                Melon.bounce_animation.append(pygame.image.load(os.path.join("data","mellon000" + str(i + 7) + ".png")))
            Melon.bounce_animation.append(pygame.image.load(os.path.join("data","mellon0010.png")))
            Melon.bounce_animation.append(pygame.image.load(os.path.join("data","mellon0011.png")))
            
            Melon.thumbs = []
            for i in xrange(2,5):
                img = pygame.transform.scale(Melon.bounce_animation[i],(20,20))
                Melon.thumbs.append(img)
        self.image = Melon.image
        self.rect = self.image.get_rect()

        self.x, self.y = 50 + random.random() * 200, 50 + random.random() * 200
        self.dx, self.dy = 0, 0
        self.t = 0
        self.state = Melon.GROWING
        self.dead = False
        self.bounces = 0

        Melon.grow_sound.play()

        self.update()

    def __del__(self):
        if not self.state == Melon.BREAKING:
            Melon.num_melons -= 1

    def crash(self):
        if self.state != Melon.FALLING:
            return
        Melon.smash_sound.play()
        self.image = Melon.smash_animation[0]
        self.state = Melon.BREAKING
        self.dy = 0
        self.dx = 0
        self.t = 0
        Melon.num_melons -= 1

    def bounce(self):
        if self.state != Melon.FALLING or self.dy <= 0:
            return
        Melon.bounce_sound.play()
        self.image = Melon.bounce_animation[-1]
        self.bounces += 1
        self.state = Melon.BOUNCING
        self.dy = -self.dy
        self.dx = random.random() - 0.5
        self.t = 0

    def update(self):
        self.x += self.dx
        self.y += self.dy

        if self.state == Melon.GROWING:
            if self.t < 18:
                self.image = Melon.grow_animation[self.t / 6]
            else:
                self.t = 0
                self.state = Melon.GROWING2
        elif self.state == Melon.GROWING2:
            if self.t < 6:
                self.image = Melon.grow_animation[3+self.t / 3]
            else:
                self.state = Melon.HANGING
                self.state= Melon.image
        elif self.state == Melon.BOUNCING:
            if self.t < 5:
                self.image = Melon.bounce_animation[int(random.random()*len(Melon.bounce_animation)-1)]
            # Combo counter
            #elif self.t == 5:
                #self.image = self.image.copy()
                #font = pygame.font.Font(os.path.join("data","HURRYUP.TTF"),16)
                #c = (0xff,0xff,0x55)
                #bg = (0,0,0)
                #image = font.render(str(self.bounces) + "x", True, c)
                #image2 = font.render(str(self.bounces) + "x", True, bg)
                #x = self.image.get_width()/2-image.get_width() / 2
                #y = self.image.get_height()/2-image.get_height() / 2
                #self.image.blit(image2, (x+2, y+2))
                #self.image.blit(image, (x, y))
            else:
                self.state = Melon.FALLING
                #self.dy = -1.0
        elif self.state == Melon.BREAKING:
            if self.t > 5:
                self.image = Melon.smash_animation[1]
            if self.t > 30:
                self.t = 0
                self.dead = True
        elif self.state == Melon.FALLING:
            self.dy += 0.1
        elif self.state == Melon.JIGGLING:
            self.image = pygame.transform.rotate(Melon.image, random.random() * 10 - 5)
            if self.t > 30:
                Melon.fall_sound.play()
                self.state = Melon.FALLING
                self.t = 0
        else:
            if self.t > self.timer:
                self.t = 0
                self.state = Melon.JIGGLING

        self.t += 1

        self.rect.centerx = int(self.x)
        self.rect.centery = int(self.y)
