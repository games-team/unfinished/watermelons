watermelons
a pyweek2 post compo production
------------------------------------------------------

http://www.imitationpickles.org/melons/

a thriller game
* made in 4 hours *

pekuja - code
philhassey - sound & code
treeform - gfx & concept

use arrows
or mouse

f2 - music
f10 - fullscreen

rah rah pyweek!

-------------------------------------------------------
here's the story behind the fun ....

(20:38:22) philhassey: hey .. i want to code up a mindless little game this evening... in like an hour or two... any suggestions?  something pretty simple...
(20:48:41) Treeform: a tree
(20:48:48) Treeform: apples grow on them
(20:48:51) Treeform: and fall off
(20:49:03) Treeform: and you run around try catch them
(20:49:27) philhassey: that sounds pretty fun
(20:50:00) Treeform: wil you make it?
(20:51:01) philhassey: maybe :) i'll have to think for a minute if i really want to ...
(20:51:58) Treeform: another one lost of stick people run around
(20:52:25) Treeform: and you fly in stick plane ... and bomb them ...
(20:52:35) Treeform: lots*
(20:54:11) philhassey: hmmn, that's pretty exciting too... boy...
(20:54:36) pekuja: why sticks?
(20:56:30) philhassey: i think i might go with the apple idea...
(20:56:37) philhassey: except have watermelons coming down instead of apples.
(20:56:42) philhassey: and bounce them on a trampoline ...
(20:58:56) pekuja: like that game where you save people jumping from a burning building?
(20:59:16) philhassey: "bouncing babies" ? (dos/cga)
(20:59:21) philhassey: except better :)
(21:00:30) pekuja: never heard of bouncing babies, but I think the idea has been used in several games
(21:00:41) pekuja: one of them was a Nintendo Game & Watch game I think.
(21:01:07) philhassey: anyone want to help make this game in 60 minutes ?
(21:01:44) Treeform: i can
(21:01:52) Treeform: what do you want me to do?
(21:02:40) Treeform: problem watermelons dont gorw on trees
(21:02:48) pekuja: TreeForm_: that's no problem
(21:02:52) philhassey: :)

---------------------------------------------------------------
the truth is in the next two days after we made this game
philhassey touched it up a bit more and all that.  it got polished
off pretty nicely with global high scores, etc.
