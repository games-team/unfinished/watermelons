import pygame
from pygame.locals import *

import os
from const import *

from pgu import engine

class Menu(engine.State):
    def __init__(self,game):
        self.game = game
        
    def init(self):
        if self.game.sound:
            pygame.mixer.music.stop()
        self.quit = False
        self.cur = 0
        self.menu = ['play','high scores','web highs','web recent','about','quit']
        self.bkgr = pygame.image.load(os.path.join("data","tree.jpg")).convert()
        self.logo = pygame.image.load(os.path.join("data","logo.png")).convert_alpha()
        self.zones = []
        
        
        
    def paint(self,screen):
        screen.fill((255,255,255))
        img = self.bkgr
        img.set_alpha(128)
        screen.blit(img,(0,0))
        
        img = self.logo
        
        bg = (0,0,0)
        y = 16
        x = SW-img.get_width()
        screen.blit(img,(x,y))
        
        fnt = pygame.font.Font(os.path.join("data","HURRYUP.TTF"),36)
        
        x,y = 0,96
        fg = (0xaa,0x00,0x00)
        score = 0
        if len(self.game.high):
            score = self.game.high[0].score
        img = fnt.render('high: %d'%score,1,fg)
        img2 = fnt.render('high: %d'%score,1,bg)
        x = (SW-img.get_width())/2
        screen.blit(img2,(x+2,y+2))
        screen.blit(img,(x,y))
        y += 64
        
        self.zones = []
        n = 0
        for val in self.menu:
            c = (0xaa,0x00,0x00) #(0xff,0x55,0x55)
            if n == self.cur: c = (0xff,0x55,0x55) #(0x55,0xff,0x55)
            img = fnt.render(val,1,c)
            img2 = fnt.render(val,1,bg)
            x = (SW-img.get_width())/2
            screen.blit(img2,(x+2,y+2))
            screen.blit(img,(x,y))
            self.zones.append((n,pygame.Rect(x,y,img.get_width(),img.get_height())))
            y += 40
            n += 1
            
        fnt = pygame.font.Font(os.path.join("data","HURRYUP.TTF"),24)
        y = 480-32*2
        c = (0xff,0xff,0x55)
        for line in ['a thriller game','* made in 4 hours *']:
            img = fnt.render(line,1,c)
            img2 = fnt.render(line,1,bg)
            x = (SW-img.get_width())/2
            screen.blit(img2,(x+2,y+2))
            screen.blit(img,(x,y))
            y += 28
            
        
        pygame.display.flip()
        
    def event(self,e):
        if e.type is KEYDOWN and e.key == K_UP:
            self.cur = (self.cur-1+len(self.menu))%len(self.menu)
            self.repaint()
        if e.type is KEYDOWN and e.key == K_DOWN:
            self.cur = (self.cur+1+len(self.menu))%len(self.menu)
            self.repaint()
            
        if e.type is MOUSEMOTION:
            for n,rect in self.zones:
                if rect.collidepoint(e.pos):
                    if self.cur != n:
                        self.cur = n
                        self.repaint()
            
        if (e.type is KEYDOWN and e.key in (K_RETURN,K_ESCAPE)) or (e.type is MOUSEBUTTONDOWN):
            val = self.menu[self.cur]
            if e.type is KEYDOWN and e.key == K_ESCAPE:
                val = 'quit'
            if val == 'play':
                import game
                return game.Game(self.game)
            if val == 'quit':
                return engine.Quit(self.game)
            
            if val == 'high scores':
                return Highs(self.game)
            
            if val == 'web highs':
                return Highs(self.game,'list',val) #global
            
            if val == 'web recent':
                return Highs(self.game,'recent',val) #recent
            
            if val == 'about':
                return About(self.game)
    
class About(engine.State):
    def init(self):
        self.bkgr = pygame.image.load(os.path.join("data","tree.jpg")).convert()
    def paint(self,screen):
        screen.fill((255,255,255))
        img = self.bkgr
        img.set_alpha(128)
        screen.blit(img,(0,0))
        
        fg = (0xff,0x55,0x55)
        bg = (0,0,0)
        fnt = pygame.font.Font(os.path.join("data","HURRYUP.TTF"),48)
        text = 'about'
        img = fnt.render(text,1,fg)
        img2 = fnt.render(text,1,bg)
        y = 12
        x = (SW-img.get_width())/2
        screen.blit(img2,(x+2,y+2))
        screen.blit(img,(x,y))

        fnt = pygame.font.Font(os.path.join("data","HURRYUP.TTF"),24)
        y = 80
        c = (0,0,0)
        for line in ['a thriller game','* made in 4 hours *','','pekuja - code','philhassey - sound & code','treeform - gfx & concept','',
        'use arrows','or mouse','','f2 - music','f10 - fullscreen',
        'p - pause',
        '','rah rah pyweek!']:
            img = fnt.render(line,1,c)
            #img2 = fnt.render(line,1,bg)
            x = (SW-img.get_width())/2
            #screen.blit(img2,(x+2,y+2))
            screen.blit(img,(x,y))
            y += 26
        pygame.display.flip()
    def event(self,e):
        if (e.type is KEYDOWN and e.key in (K_RETURN,K_ESCAPE)) or e.type is MOUSEBUTTONDOWN:
            return Menu(self.game)

class _Score:
    def __init__(self,score,name,data=None):
        self.score,self.name,self.data=score,name,data

def webhighs(value):
    game,a = value
    r = []
    try:
        import socket
        socket.setdefaulttimeout(SOCKET_TIMEOUT)
        import urllib2
        f = urllib2.urlopen(HIGHSURL+"?action=%s&limit=%d"%(a,HIGHS))
        for line in f.readlines():
            score,name,datetimeon = line.split("\t")
            score = int(score)
            r.append(_Score(score,name))
        f.close()
    except:
        import sys,traceback
        e_type,e_value,e_traceback = sys.exc_info()
        print 'Traceback (most recent call last):'
        traceback.print_tb(e_traceback)
        print e_type,e_value
        
    game.data = r
            
class Wait(engine.State):
    def __init__(self,game,fnc,value,next):
        self.game,self.fnc,self.value,self.next = game,fnc,value,next
        
    def init(self):
        self.bkgr = pygame.image.load(os.path.join("data","tree.jpg")).convert()
    def paint(self,screen):
        screen.fill((255,255,255))
        img = self.bkgr
        img.set_alpha(128)
        screen.blit(img,(0,0))
        
        
        
        #fg = (0xff,0x55,0x55)
        fg = (255,255,255)
        bg = (0,0,0)
        fnt = pygame.font.Font(os.path.join("data","HURRYUP.TTF"),36)
        
        y = (SH-36*3)/2
        for line in ['connecting','to server,','please wait...']:
            text = line #self.title
            img = fnt.render(text,1,fg)
            img2 = fnt.render(text,1,bg)
            x = (SW-img.get_width())/2
            screen.blit(img2,(x+2,y+2))
            screen.blit(img,(x,y))
            y += 36
            
        pygame.display.flip()
        
        self.fnc(self.value)
        
        for e in pygame.event.get():
            pass
        
        return self.next
                
def submit(hs):
    try:
        import socket
        socket.setdefaulttimeout(SOCKET_TIMEOUT)
        import urllib2
        import urllib
        f = urllib2.urlopen(HIGHSURL+"?action=submit&score=%d&name=%s"%(hs.score,urllib.quote_plus(hs.name)))
        #yeah, our global high scores go by the "honor system" :) 
    except:
        import sys,traceback
        e_type,e_value,e_traceback = sys.exc_info()
        print 'Traceback (most recent call last):'
        traceback.print_tb(e_traceback)
        print e_type,e_value

                
class Highs(engine.State):
    def __init__(self,game,web=False,title='high scores'):
        self.game = game
        self.high = self.game.high
        self.title = title
        self.web = web
            
    def init(self):
        if self.game.sound:
            pygame.mixer.music.stop()
        
        #self.game.score = 100
        self.score = None
        if self.game.score != None and self.high.check(self.game.score) is not None:
            self.score = self.high.submit(self.game.score,'')
        self.game.score = None
        self.bkgr = pygame.image.load(os.path.join("data","tree.jpg")).convert()
        fnt = pygame.font.Font(os.path.join("data","HURRYUP.TTF"),24)
        self.digits = [fnt.render('%d'%n,1,(0,0,0)) for n in xrange(0,10)]
        
        if self.web:
            return Wait(self.game,webhighs,(self.game,self.web),self)


                
    def paint(self,screen):
        if self.web:
            self.high = self.game.data

        screen.fill((255,255,255))
        img = self.bkgr
        img.set_alpha(128)
        screen.blit(img,(0,0))
        
        fg = (0xff,0x55,0x55)
        bg = (0,0,0)
        fnt = pygame.font.Font(os.path.join("data","HURRYUP.TTF"),48)
        text = self.title
        img = fnt.render(text,1,fg)
        img2 = fnt.render(text,1,bg)
        y = 12
        x = (SW-img.get_width())/2
        screen.blit(img2,(x+2,y+2))
        screen.blit(img,(x,y))

        
        fnt = pygame.font.Font(os.path.join("data","HURRYUP.TTF"),24)
        #fnt2 = pygame.font.Font(os.path.join("data","HURRYUP.TTF"),24) #a mono font would be nice
        
        high = self.high
        
        fg = (0,0,0)
        
        x1,x2,x3 = 8,40,SW-8
        y = 80
        for n in xrange(0,min(HIGHS,len(high))):
            if n < len(high):
                hs = high[n]
                name,score =hs.name,hs.score
            else:
                name,score = '',0
                
            c = fg
            #print n,self.score
            if n is self.score:
                c = (255,0x55,0x55)
                name = name+"_"
            
            img = fnt.render("%d."%(n+1),1,c)
            screen.blit(img,(x1,y))
            
            img = fnt.render(name,1,c)
            screen.blit(img,(x2,y))
            
            text = "%d"%score
            
            tw = 14
            x = x3-len(text)*tw
            for c in text:
                img = self.digits[ord(c)-ord('0')]
                screen.blit(img,(x+(tw-img.get_width())/2,y))
                x += tw
            
            #img = fnt2.render("%d"%score,1,c)
            #screen.blit(img,(x3-img.get_width(),y))
            
            y += 25
            
        pygame.display.flip()
        
    def event(self,e):
        if (e.type is KEYDOWN and e.key in (K_RETURN,K_ESCAPE)) or e.type is MOUSEBUTTONDOWN:
            if self.score is None or len(self.high[self.score].name):
                if self.score is not None:
                    self.high.save()
                    return Wait(self.game,submit,self.high[self.score],Menu(self.game))
                return Menu(self.game)
        
        if self.score is not None:
            hs = self.high[self.score]
            if e.type is KEYDOWN and e.key == K_BACKSPACE:
                if len(hs.name): 
                    hs.name = hs.name[:-1]
                    self.repaint()
            if e.type is KEYDOWN and (e.key in xrange(K_a,K_z+1) or e.key in (K_MINUS,K_SPACE) or e.key in xrange(K_0,K_9+1)) and len(hs.name) < 12:
                hs.name = hs.name + (e.unicode).encode('latin-1')
                self.repaint()
                
                
                
                
            
        
