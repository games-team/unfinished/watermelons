import os
import pygame

from const import *

class Trampoline(pygame.sprite.Sprite):
    images = []
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)

        if len(Trampoline.images) == 0:
            Trampoline.images.append(pygame.image.load(os.path.join("data","bounce.png")))
            Trampoline.images.append(pygame.image.load(os.path.join("data","bounce2.png")))
        self.image = Trampoline.images[0]
        self.rect = self.image.get_rect()

        self.x, self.y = 160, 480 - self.image.get_height()

        self.moving_left = False
        self.moving_right = False
        self.bouncecount = 0

        self.update()

    def bounce(self):
        self.bouncecount = 5

    def update(self):
        if self.moving_left and not self.moving_right:
            self.x -= TRAMPOLINE_SPEED
        elif self.moving_right and not self.moving_left:
            self.x += TRAMPOLINE_SPEED

        if self.bouncecount > 0:
            self.image = Trampoline.images[1]
            self.bouncecount -= 1
        else:
            self.image = Trampoline.images[0]

        self.rect.left = self.x
        self.rect.top = self.y
        
        edge = 32
        self.rect.left= max(-edge,self.rect.left)
        self.rect.right = min(SW+edge,self.rect.right)
        self.x = self.rect.x

    def move_left(self, bool):
        if bool:
            self.moving_left = True
        else:
            self.moving_left = False

    def move_right(self, bool):
        if bool:
            self.moving_right = True
        else:
            self.moving_right = False
