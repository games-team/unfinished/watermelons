import os

import pygame
import math
from pygame.locals import *

from melon import Melon
from trampoline import Trampoline
from const import *
from pgu import engine
from pgu import timer

class Game(engine.State):
    NEXTFRAME = USEREVENT + 1
    def __init__(self, game):
        self.game = game
        x = Melon(123) #HACK to pre-build images

    def __del__(self):
        self.melon_sprites.empty()

    def init(self):
        pygame.mouse.set_visible(False)
        pygame.event.set_grab(True)
        self.background = pygame.image.load(os.path.join("data","tree.jpg"))

        self.melons = []
        self.melon_sprites = pygame.sprite.Group()

        self.trampoline = Trampoline()
        #self.trampoline_sprite = pygame.sprite.Group()
        #self.trampoline_sprite.add(self.trampoline)

        self.font = pygame.font.Font(os.path.join("data","HURRYUP.TTF"), 18)

        self.t = 0
        self.score = 0
        #self.combo = 0
        self.health = 10
        self.grass = pygame.image.load(os.path.join("data","grass.png")).convert_alpha()
        self.target_x = None
        if self.game.sound:
            pygame.mixer.music.load(os.path.join("data","theme.ogg"))
            pygame.mixer.music.play(-1)
        self.done = 0
        
        self.mtimer = 0 
        
        #self.combo_text = ""
        #self.combo_frame = 0
        #self.combo_surface = None
        #self.combo_rect = None

        
    def loop(self):
        
        if self.t % 100 == 0:
            self.melons.append(Melon(self.mtimer))
            self.mtimer = min(150,self.mtimer+30)
            self.melon_sprites.add(self.melons[-1])

        self.trampoline.update()
        for m in self.melons:
            m.update()
            if m.dead:
                #self.melons.remove(m)
                #self.melon_sprites.remove(m)
                pass
            else:
                if m.rect.bottom > self.screen.get_height():
                    if m.state == Melon.FALLING:
                        #self.combo = 0
                        m.crash()
                        if self.health > 0:
                            self.health -= 1
                        if self.health == 0:
                            if self.done == 0:
                                self.done = 15 #15 frame pause for animation before quit
                if m.rect.right > self.screen.get_width():
                    m.dx = -m.dx
                elif m.rect.left < 0:
                    m.dx = -m.dx

        self.trampoline.rect.top += 55
        collisions = pygame.sprite.spritecollide(self.trampoline, self.melon_sprites, False)
        self.trampoline.rect.top -= 55
        tramp = self.trampoline
        edge = 12

        #number_of_collisions = 0

        for m in collisions:
            if m.state == Melon.FALLING and m.rect.centery >= tramp.rect.centery-12 and m.rect.right-edge >= tramp.rect.left and m.rect.left+edge <= tramp.rect.right:
                self.score += int(math.pow(2, m.bounces))
                m.bounce()
                self.trampoline.bounce()
                #self.combo += 1
                #number_of_collisions += 1
        self.t += 1

        #if number_of_collisions > 1:
            # we show a combo effect here!
            #print "%sX!" % number_of_collisions
            #self.combo_text = "%sX!" % number_of_collisions
            #self.combo_frame = 0



        #the game with restrictive mouse movements...
#         if self.target_x != None:
#             if abs(self.trampoline.x - self.target_x) <= TRAMPOLINE_SPEED:
#                 self.trampoline.x = self.target_x
#                 self.target_x = None
#             elif self.trampoline.x > self.target_x:
#                 self.trampoline.x -= TRAMPOLINE_SPEED
#             else:
#                 self.trampoline.x += TRAMPOLINE_SPEED
        
        if self.done == 1:
            pygame.mouse.set_visible(True)
            pygame.event.set_grab(False)
            self.game.score = self.score
            return GameOver(self.game)
        elif self.done:
            self.done -= 1

                
    def update(self,screen):
        return self.paint(screen)
    def paint(self,screen):
        self.screen = screen

        self.screen.blit(self.background, self.screen.get_rect())
        self.screen.blit(self.trampoline.image, self.trampoline.rect)
        for m in self.melons:
            self.screen.blit(m.image, m.rect)
        #self.trampoline_sprite.draw(self.screen)
        #self.melon_sprites.draw(self.screen)

        score_text = self.font.render("Score: " + str(self.score), True, (0,0,0))
        score_text2 = self.font.render("Score: " + str(self.score), True, (0xff,0x55,0x55))
        self.screen.blit(score_text, (5, 1))
        self.screen.blit(score_text2, (4, 0))

#         health_text = self.font.render("Melons: " + str(self.health), True, (0,0,0))
#         rect = health_text.get_rect()
#         rect.right = self.screen.get_width()-4
#         self.screen.blit(health_text, rect)


        # show some text on combos.
        #if self.combo_text:
            #if not self.combo_surface:
                #self.combo_surface = self.font.render(self.combo_text, True, (0,0,0))
                #self.combo_rect = self.combo_surface.get_rect()
                #self.combo_rect.x = self.trampoline.rect.x
                #self.combo_rect.y = self.trampoline.rect.y - 20
                #self.screen.blit(self.combo_surface, self.combo_rect)
            #else:
                #self.screen.blit(self.combo_surface, self.combo_rect)


        #self.combo_frame += 1

        #if self.combo_frame > 100:
            #self.combo_text = ""
            #self.combo_surface = None
            #self.combo_frame = 0
            #self.combo_rect = None

        x = SW - 20*self.health
        y = 0
        for n in xrange(0,self.health):
            img = Melon.thumbs[n%3]
            screen.blit(img,(x,y))
            x += 20
        
        img = self.grass
        self.screen.blit(img,(0,SH-img.get_height()))

        #if self.combo > 2:
            #font = pygame.font.Font(os.path.join("data","HURRYUP.TTF"),24)
            #c = (0xff,0x55,0x55)
            #bg = (0,0,0)
            #image = font.render(str(self.combo) + "x combo", True, c)
            #image2 = font.render(str(self.combo) + "x combo", True, bg)
            #x = SW/2-image.get_width() / 2
            #y = SH/2-image.get_height() / 2
            #self.screen.blit(image2, (x+2, y+2))
            #self.screen.blit(image, (x, y))

        c = (0xff,0xff,0xff)
        bg = (0,0,0)
        img = self.font.render(str(Melon.num_melons) + " melons in play", True, c)
        img2 = self.font.render(str(Melon.num_melons) + " melons in play", True, bg)
        x = (SW-img.get_width())/2
        y = SH - img.get_height() - 4
        self.screen.blit(img2, (x+1, y+1))
        self.screen.blit(img, (x, y))

        pygame.display.flip()

    
    def event(self,e):
        if e.type == KEYDOWN:
            if e.key == K_ESCAPE:
                return Prompt(self.game,self)
            elif e.key == K_LEFT:
                self.target_x = None
                self.trampoline.move_left(True)
            elif e.key == K_RIGHT:
                self.target_x = None
                self.trampoline.move_right(True)
            elif e.key in (K_p,K_PAUSE):
                self.trampoline.move_left(False)
                self.trampoline.move_right(False)
                return Pause(self.game,self)
            #elif e.key in [K_c]:
                #print "%sX!" % 2
                #self.combo_text = "%sX!" % 2
                #self.combo_frame = 0

        elif e.type == KEYUP:
            if e.key == K_LEFT:
                self.trampoline.move_left(False)
            elif e.key == K_RIGHT:
                self.trampoline.move_right(False)
        elif e.type == MOUSEMOTION:
            if 1: #self.trampoline.moving_left == False and self.trampoline.moving_right == False:
                self.target_x = e.pos[0] - self.trampoline.rect.width / 2
                self.trampoline.x = self.target_x
                self.target_x = None
    
class Pause(engine.State):
    def __init__(self,game,next):
        self.game,self.next = game,next
        self.text = "pause"
        self.font = pygame.font.Font(os.path.join("data","HURRYUP.TTF"),42)
        
    def init(self):
        pygame.mouse.set_visible(True)
        pygame.event.set_grab(False)
        if self.game.sound:
            pygame.mixer.music.pause()

    def paint(self,screen):
        self.bkgr = pygame.Surface((SW,SH))
        self.screen = screen
        self.bkgr.blit(screen,(0,0))
        img = self.font.render(self.text,1,(0,0,0))
        x,y = (SW-img.get_width())/2,(SH-img.get_height())/2
        b = 2
        for dx,dy in [(1,1)]:
            screen.blit(img,(x+dx*b,y+dy*b))
        img = self.font.render(self.text,1,(255,255,255))
        screen.blit(img,(x,y))
        pygame.display.flip()
        
    def event(self,e):
        if e.type is KEYDOWN and e.key in (K_p,K_PAUSE):
            pygame.mouse.set_visible(False)
            pygame.event.set_grab(True)
            if self.game.sound:
                pygame.mixer.music.unpause()
            return self.next
        
class Prompt(engine.State):
    def __init__(self,game,next):
        self.game,self.next = game,next
        self.text = "quit? y/n"
        
    def init(self):
        pygame.mouse.set_visible(True)
        pygame.event.set_grab(False)
        self.font = pygame.font.Font(os.path.join("data","HURRYUP.TTF"),42)

    def paint(self,screen):
        self.bkgr = pygame.Surface((SW,SH))
        self.screen = screen
        self.bkgr.blit(screen,(0,0))
        img = self.font.render(self.text,1,(0,0,0))
        x,y = (SW-img.get_width())/2,(SH-img.get_height())/2
        b = 2
        for dx,dy in [(1,1)]:
            screen.blit(img,(x+dx*b,y+dy*b))
        img = self.font.render(self.text,1,(255,255,255))
        screen.blit(img,(x,y))
        pygame.display.flip()

    def event(self,e):
        if e.type is KEYDOWN and e.key == K_y:
            pygame.mouse.set_visible(False)
            pygame.event.set_grab(True)
            self.next.done = True
            self.screen.blit(self.bkgr,(0,0))
            return self.next
        if e.type is KEYDOWN and e.key == K_n:
            pygame.mouse.set_visible(False)
            pygame.event.set_grab(True)
            self.screen.blit(self.bkgr,(0,0))
            return self.next

        
class GameOver(engine.State):
    def paint(self,screen):
        y = SH/2-48
        fg = (255,255,255)
        bg = (0,0,0)
        for text in ['game over','press enter']:
            fnt = pygame.font.Font(os.path.join("data","HURRYUP.TTF"),42)
            img = fnt.render(text,1,fg)
            img2 = fnt.render(text,1,bg)
            x = (SW-img.get_width())/2
            screen.blit(img2,(x+2,y+2))
            screen.blit(img,(x,y))
            y += 48
        pygame.display.flip()
        
    def event(self,e):
        if (e.type is KEYDOWN and e.key in (K_RETURN,K_ESCAPE)) or (e.type is MOUSEBUTTONDOWN):
            import menu
            #return menu.Menu(self.game)
            return menu.Highs(self.game)
